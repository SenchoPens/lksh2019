#include <bits/stdc++.h>
#include <limits>

using ll = long long;
using vll = std::vector<ll>;
using Edge = std::tuple<ll, ll, ll>;
using Edges = std::vector<Edge>;

const ll NEG_INF = std::numeric_limits<ll>::min();

class DSU {
  private:
    vll anc;
    vll size;
  public:
    DSU(ll n);
    ll find_root(ll v);
    void join(ll v, ll u);
};

DSU::DSU(ll n) {
  anc.resize(n);
  size.resize(n);
  std::iota(anc.begin(), anc.end(), 0);
  size.assign(n, 1);
}

ll DSU::find_root(ll v) {
  if (anc[v] == v)
    return v;
  anc[v] = find_root(anc[v]);
  return anc[v];
}

void DSU::join(ll v, ll u) {
  ll rv = find_root(v);
  ll ru = find_root(u);
  if (rv == ru)
    return;
  if (size[rv] < size[ru])
    std::swap(rv, ru);
  anc[ru] = rv;
  size[rv] += size[ru];
}

int main() {
  ll n, k;
  std::cin >> n >> k;
  
  Edges edges(k);
  for (ll i = 0; i < k; i++) {
    ll v, u, w;
    std::cin >> v >> u >> w;
    v--; u--;
    edges[i] = std::make_tuple(w, v, u);
  }

  DSU dsu(n);
  std::sort(edges.begin(), edges.end());
  ll max = NEG_INF;
  for (const Edge& e : edges) {
    ll v, u, w;
    std::tie(w, v, u) = e;
    if (dsu.find_root(v) == dsu.find_root(u))
      continue;
    max = std::max(max, w);
    dsu.join(v, u);
  }
  std::cout << max << "\n";
}

