#include <bits/stdc++.h>
#include <limits>

using ll = long long;
using Parameters = std::array<ll, 2>;

const ll NEG_INF = std::numeric_limits<ll>::min();

struct Node {
    Parameters value;
    ll prior;

    Parameters max;
    ll size;
    ll ni;

    Node* left;
    Node* right;

    Node(ll p1, ll p2, ll i) {
        value = {p1, p2};
        prior = rand();

        size = 1;
        max = {p1, p2};
        ni = i;

        left = nullptr;
        right = nullptr;
    }
};

ll get_size(Node* t) {
    if (t == nullptr)
        return 0;
    return t->size;
}

ll get_max(Node* t, int pi) {
    if (t == nullptr)
        return NEG_INF;
    return t->max[pi];
}

void update(Node* t) {
    if (t == nullptr)
        return;
    t->size = get_size(t->left) + get_size(t->right) + 1;
    for (int i = 0; i < 2; i++) {
        t->max[i] = std::max(std::max(get_max(t->left, i), get_max(t->right, i)), t->value[i]);
    }
}

void split(Node* t, ll k, Node*& l, Node*& r) {
    if (t == nullptr) {
        l = r = nullptr;
        return;
    }

    if (get_size(t->left) >= k) {
        r = t;
        split(t->left, k, l, r->left);
    } else {
        l = t;
        split(t->right, k - get_size(t->left) - 1, l->right, r);
    }

    update(l);
    update(r);
}


Node* merge(Node* l, Node* r) {
    if (l == nullptr || r == nullptr)
        return l == nullptr ? r : l;

    if (l->prior > r->prior) {
        l->right = merge(l->right, r);
        update(l);
        return l;
    } else {
        r->left = merge(l, r->left);
        update(r);
        return r;
    }
}

ll find_i(Node *t, ll k, int pi) {
    if (get_max(t, pi) < k)
        return get_size(t) + 1;
    if (get_max(t->left, pi) < k) {
        if (t->value[pi] < k) {
            return get_size(t->left) + 1 + find_i(t->right, k, pi);
        }
        return get_size(t->left) + 1;
    }
    return find_i(t->left, k, pi);
}

void print_tree(Node* t) {
    if (t == nullptr)
        return;
    print_tree(t->left);
    std::cout << t->ni + 1 << " ";
    print_tree(t->right);
}

Node* insert(Node* t, Node* v, int pi) {
    Node* l = nullptr;
    Node* r = nullptr;
    ll i = find_i(t, v->value[pi], pi);
    split(t, i - 1, l, r);
    Node* res = merge(l, merge(v, r));
    return res;
}

int main() {
    Node* t = nullptr;

    ll n;
    std::cin >> n;

    for (ll i = 0; i < n; i++) {
        ll id1, id2;
        int pi;
        std::cin >> id1 >> id2 >> pi;
        Node* v = new Node(id1, id2, i);
        t = insert(t, v, pi - 1);
    }
    print_tree(t);
    std::cout << "\n";
}

