#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;
using Query = std::tuple<bool, ll, ll>;
using Queries = std::vector<Query>;


class DSU {
    vll anc;
    vll size;
  public:
    DSU(ll n);
    ll find_root(ll v);
    void join(ll v, ll u);
};

DSU::DSU(ll n) {
  anc.resize(n);
  size.resize(n);
  std::iota(anc.begin(), anc.end(), 0);
  size.assign(n, 1);
}

ll DSU::find_root(ll v) {
  if (v == anc[v])
    return v;
  anc[v] = find_root(anc[v]);
  return anc[v];
}

void DSU::join(ll v, ll u) {
  ll r1 = find_root(v);
  ll r2 = find_root(u);
  if (r1 == r2)
      return;
  if (size[r1] < size[r2])
      std::swap(r1, r2);
  anc[r2] = r1;
  size[r1] += size[r2];
}

int main() {
  ll n, m, k;
  std::cin >> n >> m >> k;
  
  for (ll i = 0; i < m; i++) {
    ll v, u;
    std::cin >> v >> u;
  }

  Queries queries(k);
  for (ll i = k - 1; i >= 0; i--) {
    std::string type;
    ll v, u;
    std::cin >> type >> v >> u;
    v--; u--;
    if (type == "ask") {
      queries[i] = std::make_tuple(true, v, u);
    } else {
      queries[i] = std::make_tuple(false, v, u);
    }
  }

  std::vector<bool> ans(0);
  DSU dsu(n);
  std::cout << "hi" << std::endl;
  for (const auto& q : queries) {
    bool type;
    ll v, u;
    std::tie(type, v, u) = q;
    std::cout << type << " " << v << " " << u << std::endl;
    if (type) {
      ans.push_back(dsu.find_root(v) == dsu.find_root(u));
    } else {
      dsu.join(v, u);
    }
    std::cout << "end" << std::endl;
  }
  for (ll i = ans.size() - 1; i >= 0; i--) {
    std::cout << (ans[i] ? "YES" : "NO");
  }
}
