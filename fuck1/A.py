from sys import setrecursionlimit


setrecursionlimit(10 ** 7)
vs = set()


def make_g(n):
    return [set() for _ in range(n)]


def dfs(v, g, not_used, tin, fup, t, p, en):
    if v not in not_used:
        return
    not_used.remove(v)

    tin[v] = t
    fup[v] = t
    t += 1

    c = 0
    for u in g[v]:
        if u == p:
            continue
        if u not in not_used:
            fup[v] = min(fup[v], tin[u])
        else:
            dfs(u, g, not_used, tin, fup, t, v, en)
            c += 1
            fup[v] = min(fup[v], fup[u])
            if fup[u] >= tin[v]:
                vs.add(v)
    return c


def main():
    global vs

    n, m = map(int, input().split())
    g = make_g(n)
    en = dict()
    for i in range(1, m + 1):
        v, u = map(int, input().split())
        v -= 1
        u -= 1
        g[v].add(u)
        g[u].add(v)
        en[(v, u)] = i
        en[(u, v)] = i

    fup = [float('inf') for _ in range(n)]
    tin = [0] * n

    not_used = set(range(n))
    t = 0
    while not_used:
        s = not_used.pop()
        not_used.add(s)
        c = dfs(s, g, not_used, tin, fup, t, -1, en)
        if c < 2:
            vs.discard(s)
    vs = sorted(vs)
    print(len(vs))
    print(' '.join(str(x + 1) for x in vs))


main()
