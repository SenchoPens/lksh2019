#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;

class SegmentTree {
  private:
    vll tree;
    vll a;
    vll add;

    void push(ll v);
  public:
    SegmentTree(ll n);
    void add(ll l, ll r, ll x, ll v, ll vl, ll vr);
    ll get(ll i);
};
