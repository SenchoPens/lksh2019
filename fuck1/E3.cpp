#include <bits/stdc++.h>
#include <climits>

using ll = long long;

const ll INF = LLONG_MAX;

struct Node {
  ll value;
  ll prior;

  ll size;
  ll min;
  bool reverse;

  Node* left;
  Node* right;

  Node(ll x) {
    value = x;
    prior = rand();

    size = 1;
    min = value;
    reverse = false;

    left = nullptr;
    right = nullptr;
  }
};

ll get_size(Node* t) {
  if (t == nullptr)
    return 0;
  return t->size;
}

ll get_min(Node* t) {
  if (t == nullptr)
    return INF;
  return t->min;
}

void update(Node* t) {
  if (t == nullptr)
    return;
  t->size = get_size(t->left) + get_size(t->right) + 1;
  t->min = std::min(std::min(get_min(t->left), get_min(t->right)), t->value);
}

void reverse_node(Node* t, bool reverse) {
  if (t == nullptr)
    return;
  t->reverse = t->reverse != reverse;
}

void push(Node* t) {
  if (t == nullptr)
    return;
  reverse_node(t->left, t->reverse);
  reverse_node(t->right, t->reverse);
  if (t->reverse) {
    std::swap(t->left, t->right);
    t->reverse = false;
  }
}

void split(Node* t, ll k, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }

  push(t);

  if (get_size(t->left) >= k) {
    r = t;
    split(t->left, k, l, r->left);
  } else {
    l = t;
    split(t->right, k - get_size(t->left) - 1, l->right, r);
  }

  update(l);
  update(r);
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;

  push(l); push(r);

  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    update(l);
    return l;
  } else {
    r->left = merge(l, r->left);
    update(r);
    return r;
  }
}

void split_segment(ll li, ll ri, Node* t, Node*& l, Node*& r, Node*& rl, Node*& rr) {
    split(t, li - 1, l, r);
    split(r, ri - li + 1, rl, rr);
}

int main() {
  Node* t = nullptr;

  ll n, m;
  std::cin >> n >> m;

  for (ll i = 0; i < n; i++) {
    ll value;
    std::cin >> value;
    Node* v = new Node(value);
    t = merge(t, v);
  }

  for (ll i = 0; i < m; i++) {
    ll type, li, ri;
    std::cin >> type >> li >> ri;

    Node* l = nullptr;
    Node* r = nullptr;
    Node* rl = nullptr;
    Node* rr = nullptr;

    split_segment(li, ri, t, l, r, rl, rr);

    if (type == 1) {
      reverse_node(rl, true);
    } else {
      std::cout << get_min(rl) << "\n";
    }

    t = merge(l, merge(rl, rr));
  }
}


