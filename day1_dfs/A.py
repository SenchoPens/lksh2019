from collections import defaultdict
from sys import setrecursionlimit


setrecursionlimit(10 ** 7)


def make_g(n):
    return [set() for _ in range(n)]


def make_used(n):
    return [False] * n


def dfs1(v, g, not_used, order):
    if v not in not_used:
        return
    not_used.remove(v)
    for u in g[v]:
        dfs1(u, g, not_used, order)
    order.append(v)


def dfs2(v, g, condensed_i, c, cg):
    if condensed_i[v] != -1:
        return
    condensed_i[v] = c
    for u in g[v]:
        ciu = condensed_i[u]
        if -1 < ciu < c:
            cg[c].add(ciu)
        else:
            dfs2(u, g, condensed_i, c, cg)


def reverse_g(g):
    n = len(g)
    rg = make_g(n)
    for v in range(n):
        for u in g[v]:
            rg[u].add(v)
    return rg


def main():
    n, m = map(int, input().split())
    g = make_g(n)
    for _ in range(m):
        v, u = map(int, input().split())
        v -= 1
        u -= 1
        g[v].add(u)

    not_used = set(range(n))
    order = []
    while not_used:
        s = not_used.pop()
        not_used.add(s)
        dfs1(s, g, not_used, order)

    rg = reverse_g(g)
    cg = defaultdict(set)
    condensed_i = [-1] * n
    c = 0
    for v in order[::-1]:
        if condensed_i[v] == -1:
            dfs2(v, rg, condensed_i, c, cg)
            c += 1

    print(sum(len(x) for x in cg.values()))


main()
