from sys import setrecursionlimit
import array


setrecursionlimit(10 ** 7)

COLOR = 0
DUPLICATED_EDGES = set()


def make_g(n):
    return [list() for _ in range(n)]


def paint(stack, v, component):
    global COLOR

    COLOR += 1
    while stack:
        u = stack.pop()
        component[u] = COLOR
        if u == v:
            break


def dfs(v, g, not_used, tin, fup, t, p, stack, component):
    if v not in not_used:
        return
    not_used.remove(v)
    stack.append(v)

    tin[v] = t
    fup[v] = t
    t += 1

    for u in g[v]:
        if u == p:
            continue
        if u not in not_used:
            fup[v] = min(fup[v], tin[u])
        else:
            dfs(u, g, not_used, tin, fup, t, v, stack, component)
            fup[v] = min(fup[v], fup[u])
            if fup[u] > tin[v] and (v, u) not in DUPLICATED_EDGES:
                paint(stack, u, component)

    return stack


def main():
    global DUPLICATED_EDGES

    n, m = map(int, input().split())
    g = make_g(n)
    all_edges = set()
    for i in range(m):
        v, u = map(int, input().split())
        v -= 1
        u -= 1
        v, u = min(v, u), max(v, u)
        if (v, u) in all_edges:
            DUPLICATED_EDGES.add((v, u))
            DUPLICATED_EDGES.add((u, v))
        all_edges.add((v, u))
        g[v].append(u)
        g[u].append(v)
    del all_edges

    fup = [float('inf') for _ in range(n)]
    component = [COLOR] * n
    tin = [0] * n

    not_used = set(range(n))
    t = 0
    while not_used:
        s = not_used.pop()
        not_used.add(s)
        rest_of_stack = dfs(s, g, not_used, tin, fup, t, -1, [], component)
        if rest_of_stack:
            paint(rest_of_stack, -1, component)

    print(COLOR)
    print(' '.join(map(str, component)))


main()
