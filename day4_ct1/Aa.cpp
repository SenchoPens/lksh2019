#include <bits/stdc++.h>

struct Node {
  int key;
  unsigned int prior;
  int vi;
  Node* left;
  Node* right;

  Node(int x, unsigned int y, int i) {
    left = nullptr;
    right = nullptr;
    key = x;
    prior = y;
    vi = i;
  }
};

void split(Node* t, int x, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }
  if (t->key > x) {
    r = t;
    split(t->left, x, l, r->left);
  } else {
    l = t;
    split(t->right, x, r, l->right);
  }
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;
  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    return l;
  } else {
    r->left = merge(l, r->left);
    return r;
  }
}

Node* naive_insert(Node* t, Node* v) {
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, v->key, l, r);
  t = merge(l, v);
  t = merge(t, r);
  return t;
}

int print_tree(Node* t, int pi) {
  if (t == nullptr)
    return 0;
  int li = print_tree(t->left, t->vi);
  int ri = print_tree(t->right, t->vi);
  std::cout << t->vi << " | " << t->key << " " << t->prior << " | " << pi << " " << li << " " << ri << "\n";
  return t->vi;
}

int main() {
  int n;
  std::cin >> n;

  Node* t = nullptr;
  for (int i = 0; i < n; i++) {
    int a, b;
    std::cin >> a >> b;

    Node* node = new Node(a, b, i + 1);
    t = naive_insert(t, node);
  }

  std::cout << "YES\n";
  print_tree(t, 0);
}

