#include <bits/stdc++.h>

using ll = long long;

const ll MOD = 1000000000;

struct Node {
  ll value;
  ll prior;

  ll size;

  Node* left;
  Node* right;

  Node(ll x) {
    value = x;
    prior = rand();

    size = 1;

    left = nullptr;
    right = nullptr;
  }
};

ll get_size(Node* t) {
  if (t == nullptr)
    return 0;
  return t->size;
}

void update(Node* t) {
  if (t == nullptr)
    return;
  t->size = get_size(t->left) + get_size(t->right) + 1;
}

void split(Node* t, ll x, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }

  if (t->value > x) {
    r = t;
    split(t->left, x, l, r->left);
  } else {
    l = t;
    split(t->right, x, l->right, r);
  }

  update(l);
  update(r);
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;

  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    update(l);
    return l;
  } else {
    r->left = merge(l, r->left);
    update(r);
    return r;
  }
}

bool exists(Node* t, ll x) {
  if (t == nullptr)
    return false;
  if (t->value == x)
    return true;
  if (t->value < x)
    return exists(t->right, x);
  return exists(t->left, x);
}

ll get(Node* t, ll i) {
  if (get_size(t->left) >= i)
    return get(t->left, i);
  if (get_size(t->left) + 1 == i) 
    return t->value;
  return get(t->right, i - get_size(t->left) - 1);
}

Node* naive_insert(Node* t, ll x) {
  if (exists(t, x))
    return t;

  Node* v = new Node(x);
  Node* l = nullptr;
  Node* r = nullptr;

  split(t, v->value, l, r);

  return merge(merge(l, v), r);
}

Node* naive_erase(Node* t, int x) {
  if (!exists(t, x))
    return t;
  Node* l = nullptr;
  Node* r = nullptr;
  Node* m = nullptr;
  split(t, x - 1, l, r);
  split(r, x, m, r);
  delete m;
  return merge(l, r);
}

int main() {
  int n;
  std::cin >> n;
  Node* t = nullptr;

  for (int i = 0; i < n; i++) {
    ll k;
    int command;
    std::cin >> command >> k;

    if (command == 1) {
      t = naive_insert(t, k);
    } else if (command == -1) {
      t = naive_erase(t, k);
    } else if (command == 0) {
      ll i = get_size(t) - k + 1;
      std::cout << get(t, i) << "\n";
    }
  }
}

