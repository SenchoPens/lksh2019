#include <bits/stdc++.h>

using AnsLine = std::tuple<int, int, int>;
using Ans = std::vector<AnsLine>;

struct Node {
  int key;
  int prior;
  int vi;
  Node* left;
  Node* right;

  Node(int x, int y, int i) {
    left = nullptr;
    right = nullptr;
    key = x;
    prior = y;
    vi = i;
  }
};

void split(Node* t, int x, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }
  if (t->key > x) {
    r = t;
    split(t->left, x, l, r->left);
  } else {
    l = t;
    split(t->right, x, l->right, r);
  }
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;

  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    return l;
  } else {
    r->left = merge(l, r->left);
    return r;
  }
}

Node* naive_insert(Node* t, Node* v) {
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, v->key, l, r);
  return merge(merge(l, v), r);
}

int save_tree(Node* t, int pi, Ans& ans) {
  if (t == nullptr)
    return 0;
  int li = save_tree(t->left, t->vi, ans);
  int ri = save_tree(t->right, t->vi, ans);
  ans[t->vi - 1] = std::make_tuple(pi, li, ri);
  return t->vi;
}

int print_tree(Node* t, int pi) {
  if (t == nullptr)
    return 0;
  int li = print_tree(t->left, t->vi);
  int ri = print_tree(t->right, t->vi);
  std::cout << t->vi << " | " << t->key << " " << t->prior << " | " << pi << " " << li << " " << ri << "\n";
  return t->vi;
}

Node* build_tree(std::vector<Node*>& nodes) {
  std::vector<Node*> right_path(0);
  Node* t = nullptr;

  for (const auto& node : nodes) {
    Node* last = nullptr;
    while (!right_path.empty() && right_path.back()->prior < node->prior) {
      last = right_path.back();
      right_path.pop_back();
    }
    if (!right_path.empty()) {
      right_path.back()->right = node;
      node->left = last;
    } else {
      t = node;
      node->left = last;
    }
    right_path.push_back(node);

    print_tree(t, 0);
    std::cout << "\n";
  }

  return t;
}


int main() {
  int n;
  std::cin >> n;

  std::vector<std::tuple<int, int, int>> points(n);
  for (int i = 0; i < n; i++) {
    int a, b;
    std::cin >> a >> b;
    points[i] = std::make_tuple(a, b, i);
  }
  std::sort(points.begin(), points.end());

  std::vector<Node*> nodes(n);
  for (int i = 0; i < n; i++)
    nodes[i] = new Node(std::get<0>(points[i]), -std::get<1>(points[i]), std::get<2>(points[i]) + 1);

  Node* t = build_tree(nodes);

  std::cout << "YES\n";
  Ans ans(n);
  save_tree(t, 0, ans);
  for (const auto& line : ans) {
    std::cout << std::get<0>(line) << " " << std::get<1>(line) << " " << std::get<2>(line) << "\n";
  }
}

