#include <bits/stdc++.h>

using ll = long long;

const ll MOD = 1000000000;

struct Node {
  ll value;
  ll prior;

  ll sum;

  Node* left;
  Node* right;

  Node(ll x) {
    value = x;
    prior = rand();

    sum = value;

    left = nullptr;
    right = nullptr;
  }
};

ll get_sum(Node* t) {
  if (t == nullptr)
    return 0;
  return t->sum;
}

void update(Node* t) {
  if (t == nullptr)
    return;
  t->sum = get_sum(t->left) + get_sum(t->right) + t->value;
}

void split(Node* t, ll x, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }

  if (t->value > x) {
    r = t;
    split(t->left, x, l, r->left);
  } else {
    l = t;
    split(t->right, x, l->right, r);
  }

  update(l);
  update(r);
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;

  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    update(l);
    return l;
  } else {
    r->left = merge(l, r->left);
    update(r);
    return r;
  }
}

bool exists(Node* t, ll x) {
  if (t == nullptr)
    return false;
  if (t->value == x)
    return true;
  if (t->value < x)
    return exists(t->right, x);
  return exists(t->left, x);
}

Node* naive_insert(Node* t, ll x) {
  if (exists(t, x))
    return t;

  Node* v = new Node(x);
  Node* l = nullptr;
  Node* r = nullptr;

  split(t, v->value, l, r);

  return merge(merge(l, v), r);
}

void split_segment(ll li, ll ri, Node* t, Node*& l, Node*& r, Node*& rl, Node*& rr) {
    split(t, li - 1, l, r);
    split(r, ri, rl, rr);
}

int main() {
  int n;
  std::cin >> n;
  Node* t = nullptr;

  bool last_is_query = false;
  ll last_query_result;
  for (int i = 0; i < n; i++) {
    ll x;
    std::string command;
    std::cin >> command >> x;

    if (command == "+") {
      if (last_is_query) {
        x = (x + last_query_result) % MOD;
      }

      t = naive_insert(t, x);

      last_is_query = false;
    } else {
      Node* l = nullptr;
      Node* r = nullptr;
      Node* rl = nullptr;
      Node* rr = nullptr;

      ll y;
      std::cin >> y;

      split_segment(x, y, t, l, r, rl, rr);
      last_query_result = get_sum(rl);

      std::cout << last_query_result << "\n";
      t = merge(l, merge(rl, rr));

      last_is_query = true;
    }
  }
}
