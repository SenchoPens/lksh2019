#include <bits/stdc++.h>

using AnsLine = std::tuple<int, int, int>;
using Ans = std::vector<AnsLine>;

struct Node {
  int key;
  int prior;
  Node* left;
  Node* right;

  Node(int x) {
    left = nullptr;
    right = nullptr;
    key = x;
    prior = rand();
  }
};

void print_tree(Node* t, int level) {
  if (t == nullptr)
    return;
  for (int i = 0; i < level; i++)
    std::cout << " ";
  std::cout << t->key << "\n";
  print_tree(t->left, level + 1);
  print_tree(t->right, level + 1);
}

void split(Node* t, int x, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }
  if (t->key > x) {
    r = t;
    split(t->left, x, l, r->left);
  } else {
    l = t;
    split(t->right, x, l->right, r);
  }
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;

  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    return l;
  } else {
    r->left = merge(l, r->left);
    return r;
  }
}

bool exists(Node* t, int x) {
  if (t == nullptr)
    return false;
  if (t->key == x)
    return true;
  if (t->key < x)
    return exists(t->right, x);
  return exists(t->left, x);
}

Node* naive_erase(Node* t, int x) {
  if (!exists(t, x))
    return t;
  Node* l = nullptr;
  Node* r = nullptr;
  Node* m = nullptr;
  split(t, x - 1, l, r);
  split(r, x, m, r);
  delete m;
  return merge(l, r);
}

Node* naive_insert(Node* t, int x) {
  if (exists(t, x))
    return t;
  Node* v = new Node(x);
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, v->key, l, r);
  return merge(merge(l, v), r);
}

int* find_min(Node* t) {
  if (t == nullptr)
    return nullptr;
  int* res = find_min(t->left);
  if (res == nullptr)
    return &(t->key);
  return res;
}

int* find_max(Node* t) {
  if (t == nullptr)
    return nullptr;
  int* res = find_max(t->right);
  if (res == nullptr)
    return &(t->key);
  return res;
}

int* next(Node* t, int x) {
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, x, l, r);
  return find_min(r);
}

int* prev(Node* t, int x) {
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, x - 1, l, r);
  return find_max(l);
}

int main() {
  std::ifstream input("bst.in");
  std::ofstream output("bst.out");
  std::string line;
  Node* t = nullptr;
  int i = 0;
  while (std::getline(input, line)) {
    int x;
    std::string command = "";
    std::stringstream ss(line);
    ss >> command >> x;

    if (command == "insert") {
      t = naive_insert(t, x);
    } else if (command == "delete") {
      t = naive_erase(t, x);
    } else if (command == "exists") {
      output << (exists(t, x) ? "true" : "false") << "\n";
    } else if (command == "next") {
      int* res = next(t, x);
      if (res == nullptr) {
        output << "none";
      } else {
        output << (*res);
      }
      output << "\n";
    } else if (command == "prev") {
      int* res = prev(t, x);
      if (res == nullptr) {
        output << "none";
      } else {
        output << (*res);
      }
      output << "\n";
    }
    std::cout << "\n" << i << "\n";
    print_tree(t, 0);
    i++;
  }
  input.close();
  output.close();
}



