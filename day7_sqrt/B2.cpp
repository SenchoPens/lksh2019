#include <bits/stdc++.h>

using ll = long long;
using Connections = std::vector<ll>;
using Graph = std::vector<Connections>;

int main() {
  ll n, m;
  std::cin >> n >> m;
  
  Graph g(n);
  for (ll i = 0; i < m; i++) {
    ll v, u;
    std::cin >> v >> u;
    v--; u--;
    g[v].push_back(u);
    g[u].push_back(v);
  }

  std::vector<ll> color(n, -1);
  std::vector<ll> order(n);
  std::iota(order.begin(), order.end(), 0);
  std::sort(order.begin(), order.end(),
      [&g](ll v, ll u) {
        return g[v].size() > g[u].size();
      });
  ll ans = 0;
  for (ll v : order) {
    for (ll u : g[v]) {
      color[u] = v;
    }
    for (ll u : g[v]) {
      if (g[v].size() < g[u].size())
        continue;
      for (ll w : g[u]) {
        if (color[w] == v && w > v && w > u && (v > u || g[v].size() != g[u].size())) {
          ans++;
        }
      }
    }
    for (ll u : g[v]) {
      color[u] = -1;
    }
  }
  std::cout << ans << "\n";
}


