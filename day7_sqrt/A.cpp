#include <bits/stdc++.h>

using ll = long long;
const ll MAX_HEIGHT = 1000000 + 1;

using vll = std::vector<ll>;
using Queries = std::vector<std::tuple<ll, ll, ll>>;
using Heights = std::array<ll, MAX_HEIGHT>;

void read_vll(vll& v, ll n) {
  for (ll i = 0; i < n; i++) {
    std::cin >> v[i];
  }
}

void read_queries(Queries& q, ll n) {
  for (ll i = 0; i < n; i++) {
    ll l, r;
    std::cin >> l >> r;
    q[i] = std::make_tuple(l - 1, r - 1, i);
  }
}

ll update(ll prev, ll cur, ll x) {
  return -(prev * prev * x) + (cur * cur * x);
}

ll add(ll x, Heights& ks) {
  ll prev = ks[x];
  ks[x]++;
  return update(prev, ks[x], x);
}

ll del(ll x, Heights& ks) {
  ll prev = ks[x];
  ks[x]--;
  return update(prev, ks[x], x);
}

int main() {
  ll n, t;
  std::cin >> n >> t;
  
  vll as(n);
  read_vll(as, n);

  vll ans(t);

  Queries queries(t);
  read_queries(queries, t);

  ll k = static_cast<ll>(sqrt(t)) + 1;
  std::sort(queries.begin(), queries.end(),
      [k](const auto& x1, const auto& x2) {
        return std::make_pair(std::get<0>(x1) / k, std::get<1>(x1)) <
               std::make_pair(std::get<0>(x2) / k, std::get<1>(x2));
      });
  
  Heights ks;
  ks.fill(0);
  ll res = 0;
  ll cur_l = 0;
  ll cur_r = -1;
  for (const auto& q : queries) {
    ll l, r, qi;
    std::tie(l, r, qi) = q;
    while (cur_l > l) {
      cur_l--;
      res += add(as[cur_l], ks);
    }
    while (cur_r < r) {
      cur_r++;
      res += add(as[cur_r], ks);
    }
    while (cur_l < l) {
      res += del(as[cur_l], ks);
      cur_l++;
    }
    while (cur_r > r) {
      res += del(as[cur_r], ks);
      cur_r--;
    }
    ans[qi] = res;
  }

  for (ll res : ans) {
    std::cout << res << "\n";
  }
}
