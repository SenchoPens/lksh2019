#include <bits/stdc++.h>

using ll = long long;
using Connections = std::unordered_set<ll>;
using Graph = std::vector<Connections>;

int main() {
  ll n, m;
  std::cin >> n >> m;
  
  Graph g(n);
  Edges E(m);
  for (ll i = 0; i < m; i++) {
    ll v, u;
    std::cin >> v >> u;
    v--; u--;
    g[v].insert(u);
    g[u].insert(v);
    E[i] = std::make_pair(v, u);
  }

  ll ans = 0;
  for (const auto& e : E) {
    ll v = e.first; 
    ll u = e.second;
    if (g[v].size() <= g[u].size())
      std::swap(v, u);
    for (ll w : g[u]) {
      if (g[v].find(w) != g[v].end()) {
        ans++;
      }
    }
  }

  std::cout << ans / 3 << "\n";
}

