#include <bits/stdc++.h>

using ll = long long;
using Connections = std::unordered_set<ll>;
using Graph = std::vector<Connections>;
using Edges = std::vector<std::pair<ll, ll>>;
using Ans = std::set<std::tuple<ll, ll, ll>>;

int main() {
  ll n, m;
  std::cin >> n >> m;
  
  Graph g(n);
  Edges E(m);
  for (ll i = 0; i < m; i++) {
    ll v, u;
    std::cin >> v >> u;
    v--; u--;
    g[v].insert(u);
    g[u].insert(v);
    E[i] = std::make_pair(v, u);
  }

  Ans ans;
  for (const auto& e : E) {
    ll v = e.first; 
    ll u = e.second;
    if (g[v].size() <= g[u].size())
      std::swap(v, u);
    for (ll w : g[u]) {
      if (g[v].find(w) != g[v].end()) {
        std::array<ll, 3> res = {v, w, u};
        std::sort(res.begin(), res.end());
        ans.insert(std::make_tuple(res[0], res[1], res[2]));
      }
    }
  }
  std::cout << ans.size() << "\n";
}
