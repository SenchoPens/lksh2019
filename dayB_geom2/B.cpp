#include <bits/stdc++.h>

using ll = long long;
using ld = long double;
using vll = std::vector<ll>;

const ld EPS = 10e-5;

constexpr ld pi() { return std::atan(1) * 4; };

struct Point {
    ll x, y;

    Point(ll xx, ll yy) : x(xx), y(yy) {}
    Point() { x = 0; y = 0; }

    Point operator+(const Point p) {
        return Point{x + p.x, y + p.y};
    }

    Point operator-(const Point p) {
        return Point{x - p.x, y - p.y};
    }

    ll operator%(const Point p) {
        return x * p.y - y * p.x;
    }

    ll operator*(const Point p) {
        return x * p.x + y * p.y;
    }

    Point operator*(ll k) {
        return Point{x * k, y * k};
    }

    bool operator<(const Point& p) const {
        return std::make_pair(y, x) < std::make_pair(p.y, p.x);
    }
};

using Points = std::vector<Point>;
using PointSet = std::set<Point>;

ld length(Point p) {
    return std::hypot(p.x, p.y);
}

Point read_point() {
    ll x, y;
    std::cin >> x >> y;
    return Point(x, y);
}

bool is_clockwise(Point p, Point q, Point r) {
    return (q - p) % (r - q) < 0;
}

ll get_double_area(Points&& polygon, ll n) {
    polygon.push_back(polygon[0]);

    ll sum = 0;
    for (ll i = 0; i < n; i++) {
        sum += (polygon[i + 1].x - polygon[i].x) * (polygon[i].y + polygon[i + 1].y);
    }

    return std::abs(sum);
}

bool on_one_line(Point p, Point q, Point r) {
    return (q - p) % (r - q) == 0;
}

int main() {
    ll n;
    std::cin >> n;

    Points points(n);
    for (ll i = 0; i < n; i++) {
        points[i] = read_point();
    }

    PointSet pointset(points.begin(), points.end());
    Point s = *pointset.begin();

    n = pointset.size();

    points = Points(pointset.begin(), pointset.end());
    std::sort(points.begin(), points.end(), 
        [s](Point a, Point b)
            { 
                ll res = (a - s) % (b - s);
                if (res == 0)
                    return length(a - s) < length(b - s);
                return res > 0;
            });

    ll j = 2;
    while (on_one_line(points[0], points[1], points[j]))
        j++;
    Points st = {points[0], points[j - 1]};
    points.push_back(points[0]);
    for (ll i = 2; i < n + 1; i++) {
        Point a = st[st.size() - 1];
        Point b = st[st.size() - 2];
        Point p = points[i];

        while (st.size() > 2 && !is_clockwise(p, a, b)) {
            st.pop_back();
            a = st[st.size() - 1];
            b = st[st.size() - 2];
        }
        st.push_back(p);
    }
    st.pop_back();
    
    ll k = st.size();
    std::cout << k << "\n";
    for (Point p : st)
        std::cout << p.x << " " << p.y << "\n";

    ll darea = get_double_area(std::move(st), k);
    std::cout << darea / 2 << ".";
    if (darea % 2 != 0) {
        std::cout << "5";
    } else {
        std::cout << "0";
    }
    std::cout << "\n";
}

