#include <bits/stdc++.h>

using ll = long long;
using ld = long double;
using vll = std::vector<ll>;

const ld EPS = 10e-5;

constexpr ld pi() { return std::atan(1) * 4; };

struct Point {
    ll x, y;

    Point(ll xx, ll yy) : x(xx), y(yy) {}
    Point() { x = 0; y = 0; }

    Point operator+(Point p) {
        return Point{x + p.x, y + p.y};
    }

    Point operator-(Point p) {
        return Point{x - p.x, y - p.y};
    }

    ll operator%(Point p) {
        return x * p.y - y * p.x;
    }

    ll operator*(Point p) {
        return x * p.x + y * p.y;
    }

    Point operator*(ll k) {
        return Point{x * k, y * k};
    }
};

using Points = std::vector<Point>;

ld length(Point p) {
    return std::hypot(p.x, p.y);
}

bool get_ans(const Points& polygon, ll n, Point p) {
    ld sum = 0;

    for (ll i = 0; i < n; i++) {
        Point v1 = polygon[i];
        Point v2 = polygon[i + 1];
        Point u1 = v1 - p;
        Point u2 = v2 - p;
        if (std::abs(length(u1) + length(u2) - length(v1 - v2)) < EPS)
            return true;
        ld alpha = std::atan2(u1 % u2, u1 * u2);
        sum += alpha;
    }

    return (std::abs(sum - 2 * pi()) < EPS || abs(sum + 2 * pi()) < EPS);
}

Point read_point() {
    ll x, y;
    std::cin >> x >> y;
    return Point(x, y);
}

int main() {
    ll n;
    std::cin >> n;
    Point p = read_point();

    Points polygon(n + 1);
    for (ll i = 0; i < n; i++) {
        polygon[i] = read_point();
    }
    polygon[n] = polygon[0];

    std::cout << (get_ans(polygon, n, p) ? "YES" : "NO") << "\n";
}
