import math
import sys


EPS = 10 ** -5


class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @classmethod
    def from_string(cls, s):
        a, b = s.split()
        return cls(int(a), int(b))

    @classmethod
    def to_origin_point(cls, x1, y1, x2, y2):
        return cls(x2 - x1, y2 - y1)  # move vector to (0, 0)

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __mul__(self, a):
        if isinstance(a, Vector):
            return self.x * a.x + self.y * a.y
        return Vector(self.x * a, self.y * a)

    def __mod__(self, other):
        return self.x * other.y - self.y * other.x

    def __rmul__(self, a):
        return self * a

    def __sub__(self, other):
        return self + (-other)

    def __neg__(self):
        return self * (-1)

    @property
    def length(self):  # not __len__ because __len__ must return integer
        return (self.x ** 2 + self.y ** 2) ** 0.5

    @property
    def norm(self):
        return Vector(self.x / self.length, self.y / self.length)

    @property
    def angle(self):
        return math.atan2(self.y, self.x)

    def turn_90(self):
        """ Turn Vector 90 degrees counter-clockwise. """
        return Vector(-self.y, self.x)

    def __str__(self):
        return '({}; {})'.format(self.x, self.y)

    def __repr__(self):
        return str(self)


def read_vector():
    return Vector(*map(int, input().split()))


def is_clockwise(p, q, r):
    return (q - p) % (r - q) < 0


def on_one_line(p, q, r):
    return -EPS < (p - q) % (r - q) < EPS


def get_ans(polygon, n, point):
    sum_ = 0

    for i in range(n):
        v1, v2 = polygon[i], polygon[i + 1]
        u1 = v1 - point
        u2 = v2 - point
        if abs(u1.length + u2.length - (v1 - v2).length) < EPS:
            return True
        α = math.atan2(u1 % u2, u1 * u2)
        sum_ += α

    return abs(sum_ - 2 * math.pi) < EPS or abs(sum_ + 2 * math.pi) < EPS


def main():
    n, x, y = map(int, sys.stdin.readline().split())
    point = Vector(x, y)

    polygon = [Vector.from_string(sys.stdin.readline()) for _ in range(n)]
    polygon.append(polygon[0])

    if get_ans(polygon, n, point):
        sys.stdout.write('YES\n')
    else:
        sys.stdout.write('NO\n')


main()
