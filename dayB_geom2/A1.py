from typing import NamedTuple


EPS = 10 ** -10


class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def to_origin_point(x1, y1, x2, y2):
        return Vector(x2 - x1, y2 - y1)  # move vector to (0, 0)

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __mul__(self, a):
        if isinstance(a, Vector):
            return self.x * a.x + self.y * a.y
        return Vector(self.x * a, self.y * a)

    def __mod__(self, other):
        return self.x * other.y - self.y * other.x

    def __rmul__(self, a):
        return self * a

    def __sub__(self, other):
        return self + (-other)

    def __neg__(self):
        return self * (-1)

    @property
    def length(self):  # not __len__ because __len__ must return integer
        return (self.x ** 2 + self.y ** 2) ** 0.5

    @property
    def norm(self):
        return Vector(self.x / self.length, self.y / self.length)

    def turn_90(self):
        """ Turn Vector 90 degrees counter-clockwise. """
        return Vector(-self.y, self.x)

    def __str__(self):
        return '({}; {})'.format(self.x, self.y)

    def __repr__(self):
        return str(self)


def read_vector():
    return Vector(*map(int, input().split()))


def is_clockwise(p, q, r):
    return (q - p) % (r - q) < 0


def on_one_line(p, q, r):
    return -EPS < (p - q) % (r - q) < EPS


def intersect(p, v, u):
    if v.y == u.y == p.y:
        if v.x < u.x:
            return v.x < p.x <= u.x
        else:
            return u.x <= p.x < v.x

    if v.y <= p.y or u.y > p.y:
        return False

    if v.x == u.x:
        return p.x <= u.x

    return not is_clockwise(v, p, u)


def main():
    n, x, y = map(int, input().split())
    point = Vector(x, y)

    polygon = [Vector(*map(int, input().split())) for _ in range(n)]
    polygon.append(polygon[0])

    edges = []
    for i in range(n):
        v1, v2 = polygon[i], polygon[i + 1]
        if v1.y > v2.y:
            edges.append((v1, v2))
        else:
            edges.append((v2, v1))

    intersection_count = 0
    for v1, v2 in edges:
        if intersect(point, v1, v2):
            intersection_count += 1

    if intersection_count % 2 == 1:
        print('YES')
    else:
        print('NO')


main()
