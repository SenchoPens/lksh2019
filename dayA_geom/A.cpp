#include <bits/stdc++.h>

using ld = long double;
constexpr ld pi() { return std::atan(1)*4; }

struct Vector {
    ld x;
    ld y;

    Vector(ld xx, ld yy) : x(xx), y(yy) {};
};

ld angle(Vector v) {
    ld a = std::atan2(v.y, v.x);
    if (a < 0) {
        return a + 2.0 * pi();
    }
    return a;
}

int main() {
    std::cout << std::fixed;
    std::cout << std::setprecision(20);

    ld x, y;

    std::cin >> x >> y;
    std::cout << angle(Vector(x, y)) << "\n";
}
