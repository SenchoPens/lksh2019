#include <bits/stdc++.h>

using ld = long double;

int main() {
    std::cout << std::fixed;
    std::cout << std::setprecision(6);

    ld x1, y1, x2, y2;

    std::cin >> x1 >> y1 >> x2 >> y2;

    ld a = y1 - y2;
    ld b = x2 - x1;
    ld c = -x2 * y1 + y2 * x1;

    std::cout << a << " " << b << " " << c << "\n";
}

