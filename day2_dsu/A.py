class DSU:
    def __init__(self, n):
        self.anc = list(range(n))
        self.size = [1] * n

    def find_root(self, v):
        r = v
        while r != self.anc[r]:
            r = self.anc[r]
        self.anc[v] = r
        return r

    def join(self, v, u):
        r1 = self.find_root(v)
        r2 = self.find_root(u)
        if r1 == r2:
            return
        if self.size[r1] < self.size[r2]:
            r1, r2 = r2, r1
        self.anc[r2] = r1
        self.size[r1] += self.size[r2]


def main():
    input_file = open('mst.in', 'r')
    n, m = map(int, input_file.readline().split())

    edges = []
    for _ in range(m):
        b, e, w = map(int, input_file.readline().split())
        b -= 1
        e -= 1
        edges.append((w, b, e))

    edges.sort()
    dsu = DSU(n)
    ans = 0

    for edge in edges:
        w, b, e = edge
        if dsu.find_root(b) != dsu.find_root(e):
            ans += w
            dsu.join(b, e)

    output_file = open('mst.out', 'w')
    output_file.write(str(ans))


main()
