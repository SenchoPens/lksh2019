#include <bits/stdc++.h>

using namespace std;
using Edge = tuple<int, int, int>;

class DSU {
    vector<int> anc;
    vector<int> size;
  public:
    DSU(int n);
    int find_root(int v);
    void join(int v, int u);
};

DSU::DSU(int n) {
  anc.resize(n);
  size.resize(n);
  iota(anc.begin(), anc.end(), 0);
  size.assign(n, 1);
}

int DSU::find_root(int v) {
  if (v == anc[v])
    return v;
  anc[v] = find_root(anc[v]);
  return anc[v];
}

void DSU::join(int v, int u) {
  int r1 = find_root(v);
  int r2 = find_root(u);
  if (r1 == r2)
      return;
  if (size[r1] < size[r2])
      swap(r1, r2);
  anc[r2] = r1;
  size[r1] += size[r2];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);

  int n, m;

  cin >> n >> m;

  vector<Edge> edges(m);
  for (int i = 0; i < m; i++) {
    int b, e, w;
    cin >> b >> e >> w;
    b -= 1;
    e -= 1;
    edges.push_back(make_tuple(w, b, e));
  }

  sort(edges.begin(), edges.end());
  DSU dsu(n);
  long long ans;
  ans = 0;

  for (Edge e : edges) {
    if (dsu.find_root(get<1>(e)) != dsu.find_root(get<2>(e))) {
      ans += get<0>(e);
      dsu.join(get<1>(e), get<2>(e));
    }
  }

  cout << ans;
}
