#include <bits/stdc++.h>

using namespace std;
using Coordinate = pair<int, int>;

const double INF = 100000000.0;

double dist(Coordinate a, Coordinate b) {
  return sqrt(pow(a.first - b.first, 2) + pow(a.second - b.second, 2));
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);

  int n;
  cin >> n;

  vector<Coordinate> cities;
  vector<bool> used(n);
  for (int i = 0; i < n; i++) {
    int x, y;
    cin >> x >> y;
    Coordinate coord = make_pair(x, y);
    cities.push_back(coord);
  }

  double ans = 0;

  vector<double> p(n);
  p.assign(n, INF);
  p[0] = 0;
  for (int i = 0; i < n; i++) {
    int v = -1;
    for (int j = 0; j < n; j++) {
      if (!used[j] && (v == -1 || p[j] < p[v]))
        v = j;
    }
  
    used[v] = true;
    ans += p[v];

    for (int u = 0; u < n; u++) {
      double vu_dist = dist(cities[v], cities[u]);
      if (vu_dist < p[u])
        p[u] = vu_dist;
    }
  }

  cout << fixed;
  cout << setprecision(4);
  cout << ans << endl;
}

