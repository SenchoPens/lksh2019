#include <bits/stdc++.h>

using namespace std;
using Vertex = array<int, 3>;

void print_vertex(Vertex v) {
  cout << v[0] << " " << v[1] << " " << v[2];
}

int vertex_to_int(Vertex v, int n, int m, int k) {
  return (v[0] - 1) * m * k + (v[1] - 1) * k + (v[2] - 1);
}

class DSU {
    vector<int> anc;
    vector<int> size;
  public:
    DSU(int n);
    int find_root(int v);
    void join(int v, int u);
};

DSU::DSU(int n) {
  anc.resize(n);
  size.resize(n);
  iota(anc.begin(), anc.end(), 0);
  size.assign(n, 1);
}

int DSU::find_root(int v) {
  if (v == anc[v])
    return v;
  anc[v] = find_root(anc[v]);
  return anc[v];
}

void DSU::join(int v, int u) {
  int r1 = find_root(v);
  int r2 = find_root(u);
  if (r1 == r2)
      return;
  if (size[r1] < size[r2])
      swap(r1, r2);
  anc[r2] = r1;
  size[r1] += size[r2];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);

  int n, m, k, q;

  cin >> n >> m >> k >> q;
  map<Vertex, int> vi;
  int c = 0;
  for (int i = 1; i < n + 1; i++) {
    for (int j = 1; j < m + 1; j++) {
      for (int p = 1; p < k + 1; p++) {
        vi[{i, j, p}] = c;
        c++;
      }
    }
  }

  DSU dsu = DSU(n * m * k);

  for (int i = 0; i < q; i++) {
    int x1, y1, z1, x2, y2, z2;
    cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
    int move[3] = {0, 0, 0};
    Vertex v1 = {x1, y1, z1};
    Vertex v2 = {x2, y2, z2};

    for (int j = 0; j < 3; j++) {
      if (v1[j] < v2[j]) {
        move[j]++;
      } else if (v1[j] > v2[j]) {
        move[j]--;
      }
    }
    
    Vertex next;
    Vertex cur;
    copy(v1.begin(), v1.end(), next.begin());
    copy(v1.begin(), v1.end(), cur.begin());
    while (cur != v2) {
      for (int j = 0; j < 3; j++)
        next[j] = next[j] + move[j];
      if (cur == v2)
        break;

      int cur_i = vertex_to_int(cur, n, m, k);
      int next_i = vertex_to_int(next, n, m, k);

      cout << cur_i << " " << next_i << endl;
      cout << vi[cur] << " " << vi[next] << endl;
      cout << endl;

      if (dsu.find_root(cur_i) == dsu.find_root(next_i)) {
        print_vertex(cur);
        cout << " ";
        print_vertex(next);
        cout << endl;
      } else {
        dsu.join(cur_i, next_i);
      }

      copy(next.begin(), next.end(), cur.begin());
    }
  }
}


