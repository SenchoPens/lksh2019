#include <iostream>
#include <vector>

using ll = long long;
using vi = std::vector<ll>;


class SegmentTree {
  private:
    ll n;
    vi tree;
    void build(vi*, ll, ll, ll);

  public:
    SegmentTree(vi*, ll);
    void update(ll, ll, ll, ll, ll);
    ll get(ll, ll, ll, ll, ll);
};

SegmentTree::SegmentTree(vi *a, ll len) {
  n = len;
  tree.assign(len * 4, 0);
  build(a, 0, 0, n);
};

void SegmentTree::build(vi *a, ll v, ll vl, ll vr) {
  if (vr - vl == 1) {
    tree[v] = (*a)[vl];
    return;
  }
        
  ll vm = (vl + vr) / 2;
  ll l_child = 2 * v + 1;
  ll r_child = 2 * v + 2;

  build(a, l_child, vl, vm);
  build(a, r_child, vm, vr);

  tree[v] = tree[l_child] + tree[r_child];
};

void SegmentTree::update(ll pos, ll new_val, ll v, ll vl, ll vr) {
  if (pos < vl || pos >= vr)
    return;

  if (vr - vl == 1) {
    tree[v] = new_val;
    return;
  }

  ll vm = (vl + vr) / 2;
  ll l_child = 2 * v + 1;
  ll r_child = 2 * v + 2;

  // test without if
  update(pos, new_val, l_child, vl, vm);
  update(pos, new_val, r_child, vm, vr);

  tree[v] = tree[l_child] + tree[r_child];
};
    
ll SegmentTree::get(ll l, ll r, ll v, ll vl, ll vr) {
  if (vr <= l || vl >= r) {
    return 0;
  }
  if (l <= vl && r >= vr) {
    return tree[v];
  }

  ll vm = (vl + vr) / 2;
  ll l_child = 2 * v + 1;
  ll r_child = 2 * v + 2;
  return get(l, r, l_child, vl, vm) + get(l, r, r_child, vm, vr);
};


int main() {
  ll n, k;
  std::cin >> n >> k;
  
  vi arr(n, 0);
  SegmentTree st(&arr, n);

  ll a, b;
  char t;
  for (ll i = 0; i < k; i++) {
    std::cin >> t >> a >> b;
    a--;
    if (t == 'A') {
      st.update(a, b, 0, 0, n);
    } else if (t == 'Q') {
      std::cout << st.get(a, b, 0, 0, n) << '\n';
    }
  }
};
