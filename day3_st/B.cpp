#include <iostream>
#include <vector>
#include <climits>

using ll = long long;
using vi = std::vector<ll>;

const ll NEGATIVE_INF = LLONG_MIN;


class SegmentTree {
  private:
    ll n;
    vi tree;
    vi add;
    void build(vi*, ll, ll, ll);
    void push(ll, ll, ll);

  public:
    SegmentTree(vi*, ll);
    void update(ll, ll, ll, ll, ll, ll);
    ll get(ll, ll, ll, ll, ll);
};


SegmentTree::SegmentTree(vi *a, ll len) {
  n = len;
  tree.assign(len * 4, 0);
  add.assign(len * 4, 0);
  build(a, 0, 0, n);
};

void SegmentTree::build(vi *a, ll v, ll vl, ll vr) {
  if (vr - vl == 1) {
    tree[v] = (*a)[vl];
    return;
  }
        
  ll vm = (vl + vr) / 2;
  ll l_child = 2 * v + 1;
  ll r_child = 2 * v + 2;

  build(a, l_child, vl, vm);
  build(a, r_child, vm, vr);

  tree[v] = std::max(tree[l_child], tree[r_child]);
};

void SegmentTree::push(ll v, ll vl, ll vr) {
  tree[v] += add[v];
  if (vr - vl != 1) {
    add[2 * v + 1] += add[v];
    add[2 * v + 2] += add[v];
  }
  add[v] = 0;
}

void SegmentTree::update(ll l, ll r, ll dt, ll v, ll vl, ll vr) {
  push(v, vl, vr);

  if (vr <= l || vl >= r)
    return;

  /*
  if (vr - vl == 1) {
    tree[v] = new_val;
    return;
  }
  */

  if (l <= vl && r >= vr) {
    add[v] += dt;
    push(v, vl, vr);
    return;
  }

  ll vm = (vl + vr) / 2;
  ll l_child = 2 * v + 1;
  ll r_child = 2 * v + 2;

  update(l, r, dt, l_child, vl, vm);
  update(l, r, dt, r_child, vm, vr);

  tree[v] = std::max(tree[l_child], tree[r_child]);
};
    
ll SegmentTree::get(ll l, ll r, ll v, ll vl, ll vr) {
  push(v, vl, vr);

  if (vr <= l || vl >= r) {
    return NEGATIVE_INF;
  }

  if (l <= vl && r >= vr) {
    return tree[v];
  }

  ll vm = (vl + vr) / 2;
  ll l_child = 2 * v + 1;
  ll r_child = 2 * v + 2;
  return std::max(get(l, r, l_child, vl, vm), get(l, r, r_child, vm, vr));
};


int main() {
  ll n, q;
  std::cin >> n >> q;
  
  vi arr(n, 0);
  for (int i = 0; i < n; i++) {
    std::cin >> arr[i];
  }
  SegmentTree st(&arr, n);

  for (ll i = 0; i < q; i++) {
    ll l, r, v;
    std::string t;
    std::cin >> t >> l >> r;
    l--;
    if (t == "add") {
      std::cin >> v;
      st.update(l, r, v, 0, 0, n);
    } else if (t == "max") {
      std::cout << st.get(l, r, 0, 0, n) << std::endl;
    }
  }
  return 0;
};

