#include <bits/stdc++.h>
#include <limits>

using ll = long long;
using vll = std::vector<ll>;

ll INF = std::numeric_limits<ll>::max();

int main() {
    ll n, a1, k, b, m;
    std::cin >> n >> a1 >> k >> b >> m;

    vll a(n);
    a[0] = a1;
    for (ll i = 1; i < n; i++) {
        a[i] = (k * a[i - 1] + b) % m;
    }

    vll lis(n + 1, INF);

    for (ll i = 0; i < n; i++) {
        auto p_it = std::lower_bound(lis.begin(), lis.end(), a[i]);
        ll l;
        if (p_it == lis.begin()) {
            l = 0;
        } else {
            l = (p_it - lis.begin());
        }
        lis[l] = std::min(lis[l], a[i]);
    }

    std::cout << (std::find(lis.begin(), lis.end(), INF) - lis.begin()) << "\n";
}
