#include <bits/stdc++.h>

using vi = std::vector<int>;
using vvi = std::vector<vi>;

int main() {
    int n;
    std::cin >> n;
    
    vi a(n);
    for (int i = 0; i < n; i++)
        std::cin >> a[i];

    vvi dp(n);
    for (int i = 0; i < n; i++) {
        dp[i].assign(n, 0);
    }

    for (int l = 2; l < n + 1; l++) {
        for (int i = 0; i < n - l + 1; i++) {
            int j = i + l - 1;
            if (l % 2 == 0) {
                dp[i][j] = std::max(dp[i + 1][j] + a[i], dp[i][j - 1] + a[j]);
            } else {
                dp[i][j] = std::min(dp[i + 1][j], dp[i][j - 1]);
            }
        }
    }

    std::cout << dp[0][n - 1] << "\n";
}
