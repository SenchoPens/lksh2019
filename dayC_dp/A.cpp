#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;
using vvll = std::vector<vll>;
using vc = std::vector<char>;
using vvc = std::vector<vc>;

int main() {
    std::string a, b;

    std::cin >> a >> b;

    ll n = a.size();
    ll m = b.size();
    
    vvll dp(n + 1);
    vvc ans(n + 1);
    for (ll i = 0; i < n + 1; i++) {
        dp[i].assign(m + 1, 0);
        ans[i].assign(m + 1, 0);
    }

    for (ll i = 1; i < n + 1; i++) {
        for (ll j = 1; j < m + 1; j++) {
            if (a[i - 1] == b[j - 1]) {
                dp[i][j] = dp[i - 1][j - 1] + 1;
                ans[i][j] = 'D';
            } else if (dp[i][j - 1] >= dp[i - 1][j]) {
                dp[i][j] = dp[i][j - 1];
                ans[i][j] = 'L';
            } else {
                dp[i][j] = dp[i - 1][j];
                ans[i][j] = 'U';
            }
        }
    }

    ll i = n;
    ll j = m;
    std::string res;
    while (i > 0 && j > 0) {
        if (ans[i][j] == 'D') {
            res.push_back(a[i - 1]);
            i--;
            j--;
        } else if (ans[i][j] == 'L') {
            j--;
        } else {
            i--;
        }
    }

    std::reverse(res.begin(), res.end());
    std::cout << res << "\n";
}
