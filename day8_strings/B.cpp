#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;

int main() {
    std::string s;
    std::cin >> s;

    ll n = s.size();
    vll p(n, 0);

    for (ll i = 1; i < n; i++) {
        ll cur = p[i - 1];
        while (cur > 0 && s[i] != s[cur]) {
            cur = p[cur - 1];
        }
        if (s[i] == s[cur])
            cur++;
        p[i] = cur;
    }

    for (ll x : p)
        std::cout << x << " ";
    std::cout << "\n";
}
