#include <bits/stdc++.h>

using ll = long long;
using ull = unsigned long long;
using vll = std::vector<ll>;
using vull = std::vector<ull>;

const ull ASCII_OFFSET = 96;

const ull P1 = 37;
const ull MOD1 = 1000000000 + 7;
const ull P2 = 43;
const ull MOD2 = 1000000000 + 9;


class HashString {
    vull hashes;
    ull p;
    ull q;
    vull ps;
    ull get_hash(ll l, ll r);
  public:
    HashString(std::string& s, ull p, ull q);
    bool is_equal(ll l1, ll r1, ll l2, ll r2);
};

HashString::HashString(std::string& s, ull pp, ull qq) : p(pp), q(qq) {
  ll n = s.size();
  hashes.assign(n + 1, 0);
  for (ll i = 1; i < n + 1; i++) {
    ull c = static_cast<ull>(s[i - 1]) - ASCII_OFFSET;
    hashes[i] = (c + hashes[i - 1] * p % q) % q;
  }

  ps.assign(n + 1, 1);
  for (ll i = 1; i < n + 1; i++) {
    ps[i] = ps[i - 1] * p % q;
  }
}

ull HashString::get_hash(ll l, ll r) {
  return (hashes[r + 1] + q - hashes[l] * ps[r - l + 1] % q) % q;
}

bool HashString::is_equal(ll l1, ll r1, ll l2, ll r2) {
  return get_hash(l1, r1) == get_hash(l2, r2);
}

int main() {
  std::string s;
  std::cin >> s;

  HashString hs1 = HashString(s, P1, MOD1);
  HashString hs2 = HashString(s, P2, MOD2);

  ll m;
  std::cin >> m;

  for (ll i = 0; i < m; i++) {
    ll a, b, c, d;
    std::cin >> a >> b >> c >> d;
    a--; b--; c--; d--;

    std::cout << ((hs1.is_equal(a, b, c, d) && hs2.is_equal(a, b, c, d)) ? "Yes" : "No");
    std::cout << "\n";
  }
}
