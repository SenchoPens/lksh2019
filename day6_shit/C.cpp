#include <bits/stdc++.h>
#include <cmath>

using ll = long long;
using vi = std::vector<ll>;
using Blocks = std::vector<vi>;

class SqrtDecomposition {
    Blocks blocks;

  public:
    SqrtDecomposition();
    void update(ll i);
    void rebuild();
};

SqrtDecomposition::SqrtDecomposition() {
}

void SqrtDecomposition::update(ll i) {
}

void SqrtDecomposition::rebuild() {
  vi arr;

  for (auto& block : blocks) {
    for (ll el : block)
      arr.push_back(el);
    block.clear();
  }

  ll sz = (ll)(sqrt(arr.size())) + 1;
  for (ll i = 0; i < arr.size() / sz; i++) {
    for (ll j = i * sz; j < (i + 1) * sz && j < arr.size(); j++) {
      blocks[i].push_back(arr[j]);
      //TODO: smth here
    }
  }
}

int main() {
  ll n;
  std::cin >> n;

  for (ll i = 0; i < n; i++) {
    std::string type;
    std::cin >> type;

    if (type == "+") {
      ll i;
      std::cin >> i;

    } else if (type == "?") {
      ll l, r;
      std::cin >> l >> r;

    }
  }
}
