#include <bits/stdc++.h>

using ll = long long;
using vi = std::vector<ll>;
using Table = std::vector<vi>;

const ll MAX_K = 25;
const ll MAX_N = 100000;

class SparseTable {
    Table table;
    vi log2;
    ll length;

  public:
    SparseTable(vi arr, ll n);
    ll get_min(ll l, ll r);
};

SparseTable::SparseTable(vi arr, ll n) {
  length = n;

  log2.assign(MAX_N + 1, 0);
  for (ll i = 2; i <= MAX_N; i++) {
    log2[i] = log2[i / 2] + 1;
  }

  table.reserve(MAX_K + 1);
  for (ll i = 0; i <= MAX_K; i++)
    table[i].assign(n, 0);

  std::copy(arr.begin(), arr.end(), table[0].begin());

  for (ll k = 1; k <= MAX_K; k++) {
    for (ll i = 0; i + (1 << k) <= n; i++) {
      table[k][i] = std::min(table[k - 1][i], table[k - 1][i + (1 << (k - 1))]);
    }
  }
}

ll SparseTable::get_min(ll l, ll r) {
  ll k = log2[r - l + 1];
  return std::min(table[k][l], table[k][r - (1 << k) + 1]);
}

void fill_arr(vi& arr, ll n) {
  for (ll i = 1; i < n; i++) {
    arr[i] = (23 * arr[i - 1] + 21563) % 16714589;
  }
}

int main() {
  ll n, m, a1;
  std::cin >> n >> m >> a1;

  vi arr(n, 0);
  arr[0] = a1;
  fill_arr(arr, n);

  SparseTable sparse_table(arr, n);

  ll u, v, ans;
  std::cin >> u >> v;

  for (ll i = 1; i <= m; i++) {
    if (u <= v) {
      ans = sparse_table.get_min(u - 1, v - 1);
    } else {
      ans = sparse_table.get_min(v - 1, u - 1);
    }
    if (i == m)
      break;
    u = ((17 * u + 751 + ans + 2 * i) % n) + 1;
    v = ((13 * v + 593 + ans + 5 * i) % n) + 1;
  }

  std::cout << u << " " << v << " " << ans << "\n";
}
