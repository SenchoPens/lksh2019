#include <bits/stdc++.h>

using ll = long long;
using vi = std::vector<ll>;

class FenwickTree {
    vi t;
    vi a;
    int length;

    int f(int x);
    int g(int x);

  public:
    FenwickTree(int n, vi a);

    ll sum(int n);
    void update(int n, ll x);
};

FenwickTree::FenwickTree(int n, vi arr) {
  length = n;
  t.assign(n, 0);
  a = arr;
}

int FenwickTree::f(int x) {
  return x & (x + 1);
}

int FenwickTree::g(int x) {
  return x | (x + 1);
}

ll FenwickTree::sum(int n) {
  ll res = 0;
  for (int i = n; i >= 0; i = f(i) - 1) {
    res += t[i];
  }
  return res;
}

void FenwickTree::update(int n, ll x) {
  ll dt = x - a[n];
  a[n] = x;
  for (int i = n; i < length; i = g(i)) {
    t[i] += dt;
  }
}

int main() {
  int n, k;
  std::cin >> n >> k;

  vi arr(n, 0);
  FenwickTree tree(n, arr);

  for (int i = 0; i < k; i++) {
    std::string t;
    std::cin >> t;
    if (t == "A") {
      int i;
      ll x;
      std::cin >> i >> x;
      tree.update(i - 1, x);
    } else if (t == "Q") {
      int l, r;
      std::cin >> l >> r;
      l--; r--;
      std::cout << tree.sum(r) - tree.sum(l - 1) << "\n";
    }
  }
}
