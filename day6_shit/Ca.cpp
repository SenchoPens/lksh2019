#include <bits/stdc++.h>
#include <cmath>

using ll = long long;
using vi = std::vector<ll>;

const ll MOD = 1000000000;

class SqrtDecomposition {
    vi blocks;
    ll sz;

  public:
    vi arr;
    SqrtDecomposition(vi arr);
    ll sum(ll i, ll j);
};

SqrtDecomposition::SqrtDecomposition(vi a) {
  arr = a;
  sz = (ll)sqrt(arr.size()) + 1;
  blocks.assign(sz, 0);
  for (ll i = 0; i < arr.size(); i++) {
    blocks[i / sz] += arr[i];
  }
}

ll SqrtDecomposition::sum(ll i, ll j) {
  ll res = 0;
  for (ll k = i; k <= j; ) {
    if (k % sz == 0 && k + sz - 1 <= j) {
      res += blocks[k / sz];
      k += sz;
    } else {
      res += arr[k];
      k += 1;
    }
  }
  return res;
}

vi merge(vi a, vi b) {
  vi new_arr(0);
  ll i = 0;
  ll j = 0;
  while (i < a.size() && j < b.size()) {
    if (a[i] < b[j]) {
      new_arr.push_back(a[i]);
      i++;
    } else if (a[i] == b[j]) {
      new_arr.push_back(a[i]);
      i++; j++;
    } else {
      new_arr.push_back(b[j]);
      j++;
    }
  }

  for (ll k = i; k < a.size(); k++)
    new_arr.push_back(a[k]);
  for (ll k = j; k < b.size(); k++)
    new_arr.push_back(b[k]);

  return new_arr;
}

void print_arr(vi arr) {
  for (ll i = 0; i < arr.size(); i++)
    std::cout << arr[i] << " ";
  std::cout << "\n";
}

int main() {
  ll n;
  std::cin >> n;

  /*
  vi arr1 = {1, 2, 3, 4, 5, 6, 8, 11, 100};
  vi arr2 = {1, 2, 3, 4, 5, 6, 7, 10};
  print_arr(merge(arr1, arr2));
  */
  /*
  SqrtDecomposition sqrt_dec(arr);
  std::cout << sqrt_dec.sum(2, 4) << "\n";
  std::cout << sqrt_dec.sum(0, 5) << "\n";
  std::cout << sqrt_dec.sum(5, 5) << "\n";
  */

  vi arr(0);

  ll step = (ll)sqrt(n) + 1;

  SqrtDecomposition sd(arr);

  std::set<ll> to_add;

  bool last_is_query = false;
  ll last_query_result;

  for (ll i = 0; i < n; i++) {
    if (i % step == 0) {
      vi to_add_vector(to_add.begin(), to_add.end());
      arr = merge(arr, to_add_vector);
      sd = SqrtDecomposition(arr);
      to_add.clear();
    }
    std::cout << "\n" << i << "\narr:\n";
    print_arr(arr);
    vi to_add_vector(to_add.begin(), to_add.end());
    print_arr(to_add_vector);
    std::cout << "/arr\n";

    std::string type;
    std::cin >> type;

    if (type == "+") {
      ll i;
      std::cin >> i;
      if (last_is_query) {
        i = (i + last_query_result) % MOD;
      }

      to_add.insert(i);

      last_is_query = false;
    } else if (type == "?") {
      ll l, r;
      std::cin >> l >> r;
      
      auto i_it = std::lower_bound(arr.begin(), arr.end(), l);
      auto j_it = std::upper_bound(arr.begin(), arr.end(), r);
      if (j_it != arr.begin()) {
        j_it--;
      } else {
        j_it = arr.end();
      }
      
      ll res = 0;
      if (i_it != arr.end() && j_it != arr.end()) {
        ll ai = i_it - arr.begin();
        ll aj = j_it - arr.begin();
        std::cout << "ai, aj: " << ai << " " << aj << "\n";
        print_arr(sd.arr);
        res = sd.sum(ai, aj);
      }

      std::cout << "adding to_add " << to_add.size() << "\n";
      for (ll el : to_add) {
        std::cout << el << " " << l << " " << r << "\n";
        if (l <= el && el <= r)
          res += el;
      }
      std::cout << "stop to_add\n";

      last_query_result = res;
      std::cout << last_query_result << "\n";
      last_is_query = true;
    }
  }
}


