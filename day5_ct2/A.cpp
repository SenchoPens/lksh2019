#include <bits/stdc++.h>

struct Node {
  int key;
  int size;
  int prior;
  Node* left;
  Node* right;

  Node(int x) {
    left = nullptr;
    right = nullptr;
    size = 1;
    key = x;
    prior = rand();
  }
};

int get_size(Node* t) {
  if (t == nullptr)
    return 0;
  return t->size;
}

void update(Node* t) {
  if (t == nullptr)
    return;
  t->size = get_size(t->left) + get_size(t->right) + 1;
}

void split(Node* t, int k, Node*& l, Node*& r) {
  if (t == nullptr) {
    l = r = nullptr;
    return;
  }

  if (get_size(t->left) >= k) {
    r = t;
    split(t->left, k, l, r->left);
  } else {
    l = t;
    split(t->right, k - get_size(t->left) - 1, l->right, r);
  }

  update(l);
  update(r);
}

Node* merge(Node* l, Node* r) {
  if (l == nullptr || r == nullptr)
    return l == nullptr ? r : l;

  if (l->prior > r->prior) {
    l->right = merge(l->right, r);
    update(l);
    return l;
  } else {
    r->left = merge(l, r->left);
    update(r);
    return r;
  }
}

bool exists(Node* t, int x) {
  if (t == nullptr)
    return false;
  if (t->key == x)
    return true;
  if (t->key < x)
    return exists(t->right, x);
  return exists(t->left, x);
}

Node* naive_erase(Node* t, int x) {
  if (!exists(t, x))
    return t;
  Node* l = nullptr;
  Node* r = nullptr;
  Node* m = nullptr;
  split(t, x - 1, l, r);
  split(r, x, m, r);
  delete m;
  return merge(l, r);
}

Node* naive_insert(Node* t, int x) {
  if (exists(t, x))
    return t;
  Node* v = new Node(x);
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, v->key, l, r);
  return merge(merge(l, v), r);
}

int* find_min(Node* t) {
  if (t == nullptr)
    return nullptr;
  int* res = find_min(t->left);
  if (res == nullptr)
    return &(t->key);
  return res;
}

int* find_max(Node* t) {
  if (t == nullptr)
    return nullptr;
  int* res = find_max(t->right);
  if (res == nullptr)
    return &(t->key);
  return res;
}

int* next(Node* t, int x) {
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, x, l, r);
  int* res = find_min(r);
  t = merge(l, r);
  return res;
}

int* prev(Node* t, int x) {
  Node* l = nullptr;
  Node* r = nullptr;
  split(t, x - 1, l, r);
  int* res = find_max(l);
  t = merge(l, r);
  return res;
}

void print_tree_as_arr(Node* t) {
  int n = get_size(t);
  for (int i = 0; i < n; i++) {
    Node* l = nullptr;
    Node* r = nullptr;
    split(t, i + 1, l, r);

    Node* m = nullptr;
    Node* r1 = nullptr;
    split(l, i, m, r1);

    std::cout << r1->key << " ";

    l = merge(m, r1);
    t = merge(l, r);
  }
  std::cout << "\n";
}

int main() {
  Node* t = nullptr;

  int n, m;
  std::cin >> n >> m;

  for (int i = 0; i < n; i++) {
    Node* v = new Node(i + 1);
    t = merge(t, v);
  }

  for (int i = 0; i < m; i++) {
    int li, ri;
    std::cin >> li >> ri;

    Node* l = nullptr;
    Node* r = nullptr;
    split(t, li - 1, l, r);

    Node* rl = nullptr;
    Node* rr = nullptr;
    split(r, ri - li + 1, rl, rr);

    t = merge(merge(rl, l), rr);
  }
  
  print_tree_as_arr(t);
}
