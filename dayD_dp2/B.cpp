#include <bits/stdc++.h>

using vi = std::vector<int>;
using vvi = std::vector<vi>;
using Mask = uint;

int main() {
    int n;
    std::cin >> n;

    //std::vector<std::vector<bool>> m(n);
    vi vs(0);
    vi us(0);
    for (int i = 0; i < n; i++) {
        std::string s;
        std::cin >> s;
        for (int j = i + 1; j < n; j++) {
            if (s[j] == 'Y') {
                vs.push_back(i);
                us.push_back(j);
            }
        }
    }
    int m = vs.size();

    std::vector<Mask> dp(1 << n, 0);

    for (Mask mask = 0; mask < (1 << n); mask++) {
        for (int i = 0; i < m; i++) {
            Mask vu_mask = (1 << vs[i]) | (1 << us[i]);
            if ((vu_mask & mask) == 0) {
                Mask new_mask = vu_mask | mask;
                dp[new_mask] = std::max(dp[new_mask], dp[mask] + 2);
            }
        }
    }

    std::cout << *std::max_element(dp.begin(), dp.end()) << "\n";

    return 0;
}
