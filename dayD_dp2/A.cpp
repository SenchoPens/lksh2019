#include <bits/stdc++.h>

using ll = long long;
using digitsA = std::array<ll, 10>;

int main() {
    int n;
    std::cin >> n;

    std::vector<digitsA> dp(n);

    dp[0].fill(1);

    for (int l = 1; l < n; l++) {
        for (int d = 0; d < 10; d++) {
            dp[l][d] = dp[l - 1][d];
            if (d > 0)
                dp[l][d] += dp[l - 1][d - 1];
            if (d < 9)
                dp[l][d] += dp[l - 1][d + 1];
        }
    }

    ll ans = 0;
    for (int d = 1; d < 10; d++)
        ans += dp[n - 1][d];
    std::cout << ans << "\n";
}
