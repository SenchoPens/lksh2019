#include <bits/stdc++.h>
#include <limits>

using Mask = uint;
using vi = std::vector<int>;

const int INF = 10e4;

int main() {
    int n;
    std::cin >> n;

    std::vector<vi> weights(n);
    for (int i = 0; i < n; i++) {
        weights[i].assign(n, 0);
        for (int j = 0; j < n; j++) {
            std::cin >> weights[i][j];
        }
    }

    uint masks_n = (1 << n);

    std::vector<vi> dp(n);
    for (int i = 0; i < n; i++) {
        dp[i].assign(masks_n, INF);
    }

    dp[0][1] = 0;
    for (Mask m = 0; m < (1 << (n - 1)); m++) {
        Mask mask = (m << 1) | 1;
        for (int v = 0; v < n; v++) {
            Mask v_mask = 1 << v;
            if ((v_mask & mask) == 0)
                continue;
            for (int u = 1; u < n; u++) {
                Mask u_mask = 1 << u;
                if (v == u || (u_mask & mask) != 0 || weights[v][u] <= 0)
                    continue;
                Mask new_mask = u_mask | mask;
                dp[u][new_mask] = std::min(dp[u][new_mask], dp[v][mask] + weights[v][u]);
            }
        }
    }

    Mask all_vs = (1 << n) - 1;
    int min_c = INF;
    int min_v = -1;
    for (int v = 0; v < n; v++) {
        if (dp[v][all_vs] < min_c) {
            min_c = dp[v][all_vs];
            min_v = v;
        }
    }
    //std::cout << min_v << " " << min_c << " " << all_vs << "\n";
    if (min_v == -1) {
        std::cout << "-1\n";
        return 0;
    }

    Mask cur_mask = ((1 << (n + 1)) - 1) ^ (1 << min_v);
    vi path = {min_v};
    int cur_cost = min_c;
    int prev_v = min_v;
    int iii = 0;
    while (cur_mask != 0) {
        int new_v = -1;
        int cost = 0;
        for (int v = 0; v < n; v++) {
            Mask v_mask = 1 << v;
            if ((v_mask & cur_mask) == 0)
                continue;
            std::cout << weights[v][prev_v] << " " << dp[v][cur_mask] << "\n";
            if (dp[v][cur_mask] == cur_cost - weights[v][prev_v]) {
                new_v = v;
                cost = weights[v][prev_v];
            }
        }
        path.push_back(new_v);
        cur_mask ^= (1 << min_v);
        cur_cost -= cost;
        prev_v = new_v;

        //std::cout << prev_v << "\n";
        std::cout << cur_cost << " " << prev_v << " " << new_v << " " << cur_mask << "\n";
        iii += 1;
        if (iii > 10) {
            return 0;
        }
    }

    std::cout << min_c << "\n";
    for (int v = n - 1; v > 0; v--) {
        std::cout << path[v] + 1 << " ";
    }
    std::cout << "\n";
}
