#include <bits/stdc++.h>

using vi = std::vector<int>;
using Queries = std::vector<std::tuple<int, int, int, int, int>>;


class FenwickTree {
    vi t;
    int length;

    int f(int x);
    int g(int x);

  public:
    FenwickTree(int n);

    int sum(int n);
    void add(int n);
    void del(int n);
};

FenwickTree::FenwickTree(int n) {
  length = n;
  t.assign(n, 0);
}

int FenwickTree::f(int x) {
  return x & (x + 1);
}

int FenwickTree::g(int x) {
  return x | (x + 1);
}

int FenwickTree::sum(int n) {
  int res = 0;
  for (int i = n; i >= 0; i = f(i) - 1) {
    res += t[i];
  }
  return res;
}

void FenwickTree::add(int n) {
  for (int i = n; i < length; i = g(i)) {
    t[i] += 1;
  }
}

void FenwickTree::del(int n) {
  for (int i = n; i < length; i = g(i)) {
    t[i] -= 1;
  }
}

void read_vi(vi& v, int n) {
  for (int i = 0; i < n; i++) {
    std::cin >> v[i];
  }
}

void read_queries(Queries& q, int n) {
  for (int i = 0; i < n; i++) {
    int x, y, k, l;
    std::cin >> x >> y >> k >> l;
    q[i] = std::make_tuple(x - 1, y - 1, k, l, i);
  }
}

int main() {
  int n, m;
  std::cin >> n >> m;
  
  vi as(n);
  read_vi(as, n);

  vi ans(m);

  Queries queries(m);
  read_queries(queries, m);

  int k = static_cast<int>(sqrt(m)) + 1;
  std::sort(queries.begin(), queries.end(),
      [k](const auto& x1, const auto& x2) {
        return std::make_pair(std::get<0>(x1) / k, std::get<1>(x1)) <
               std::make_pair(std::get<0>(x2) / k, std::get<1>(x2));
      });
  
  FenwickTree ft(n + 1);
  int cur_l = 0;
  int cur_r = -1;
  for (const auto& q : queries) {
    int l, r, x, y, qi;
    std::tie(l, r, x, y, qi) = q;
    while (cur_l > l) {
      cur_l--;
      ft.add(as[cur_l]);
    }
    while (cur_r < r) {
      cur_r++;
      ft.add(as[cur_r]);
    }
    while (cur_l < l) {
      ft.del(as[cur_l]);
      cur_l++;
    }
    while (cur_r > r) {
      ft.del(as[cur_r]);
      cur_r--;
    }
    ans[qi] = ft.sum(y) - ft.sum(x - 1);
  }

  for (int res : ans) {
    std::cout << res << "\n";
  }
}
