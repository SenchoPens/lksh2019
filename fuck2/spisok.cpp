#include <bits/stdc++.h>
#define ndp nd*
using namespace std;

struct nd{
    int prior, num, id1, ma1, id2, ma2, size;
    ndp left = nullptr;
    ndp right = nullptr;
};

using pnd = ndp;
pnd tree = nullptr;
vector <pnd> nds;
vector <pnd> rp;

int get_sz(pnd t){
    if (t == nullptr) return 0;
    else return t -> size;
}

void update(pnd t){
    if (t == nullptr) return;
    t -> ma1 = t -> id1;
    if (t -> left) t -> ma1 = max(t -> ma1, t -> left -> ma1);
    if (t -> right) t -> ma1 = max(t -> ma1, t -> right -> ma1);
    t -> ma2 = t -> id2;
    if (t -> left) t -> ma2 = max(t -> ma2, t -> left -> ma2);
    if (t -> right) t -> ma2 = max(t -> ma2, t -> right -> ma2);
    t -> size = get_sz(t -> left) + get_sz(t -> right) + 1;
}

void cout_ans(pnd t){
    if (t == nullptr) return;
    if (t -> left != nullptr) cout_ans(t -> left);
    cout << t -> num << " ";
    if (t -> right != nullptr) cout_ans(t -> right);
}

pnd merge(pnd l, pnd r){
    if (l == nullptr || r == nullptr) return l == nullptr ? r : l;
    if (l -> prior > r -> prior){
        l -> right = merge(l -> right, r);
        update(l);
        return l;
    }
    else{
        r -> left = merge(l, r -> left);
        update(r);
        return r;
    }
}

void split(pnd t, int k, pnd &l, pnd &r){
    if (t == nullptr){
        l = r = nullptr;
        return;
    }
    if (get_sz(t -> left) >= k){
        r = t;
        split(t -> left, k, l, t -> left);
        update(r);
        update(l);
    }
    else{
        l = t;
        split(t -> right, k - get_sz(t -> left) - 1, l -> right, r);
        update(l);
        update(r);
    }
}

int find1(pnd v, int k){
    if (v == nullptr) return 0;
    if (v -> left) if (v -> left -> ma1 >= k) return find1(v -> left, k);
    if (v -> id1 >= k) return get_sz(v -> left);
    else return get_sz(v -> left) + 1 + find1(v -> right, k);
}

int find2(pnd v, int k){
    if (v == nullptr) return 0;
    if (v -> left) if (v -> left -> ma2 >= k) return find2(v -> left, k);
    if (v -> id2 >= k) return get_sz(v -> left);
    else return get_sz(v -> left) + 1 + find2(v -> right, k);
}

int main(){
    int n, sz = 0, a, b, id;
    cin >> n;
    for (int i = 0; i < n; i++){
        cin >> a >> b >> id;
        pnd v = new nd();
        v -> num = i + 1;
        v -> prior = rand();
        v -> id1 = a;
        v -> ma1 = a;
        v -> id2 = b;
        v -> ma2 = b;
        v -> size = 1;
        nds.push_back(v);
        if (id == 1){
            sz = find1(tree, a);
            if (sz == i){
                tree = merge(tree, v);
            }
            else{
                pnd l, r;
                split(tree, sz, l, r);
                l = merge(l, v);
                tree = merge(l, r);
            }
        }
        else{
            sz = find2(tree, b);
            if (sz == i) tree = merge(tree, v);
            else{
                pnd l, r;
                split(tree, sz, l, r);
                l = merge(l, v);
                tree = merge(l, r);
            }
        }
    }
    cout_ans(tree);
}
