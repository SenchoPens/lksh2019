#include <bits/stdc++.h>
using namespace std;

vector<int> w;
vector<vector<int>> g, dp;

void dfs(int v){
    dp[v][1] = w[v];
    for (int u : g[v]){
        dfs(u);
        dp[v][0] += max(dp[u][0], dp[u][1]);
        dp[v][1] += dp[u][0];
    }

int main()
    int n, root;
    cin >> n;
    g.resize(n, {});
    w.resize(n);
    dp.resize(n, vector<int>(2, 0));
    for (int i = 0; i < n; ++i){
        int p, q;
        cin >> p >> q;
        w[i] = q;
        if (p != 0) g[p - 1].push_back(i);
        if (p == 0) root = i;
    }
    dfs(root);
    cout << max(dp[root][0], dp[root][1])
}
