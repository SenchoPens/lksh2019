#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;
using vvll = std::vector<vll>;

void solve(int v, vll& dp1, vll& dp2, const vvll& g, const vll& qs) {
    std::cout << v << " " << qs[v] << "\n";
    for (ll u : g[v]) {
        solve(u, dp1, dp2, g, qs);
        dp1[v] += dp2[u];
        dp2[v] += std::max(dp1[u], dp2[u]);
    }
    dp1[v] += qs[v];
}

int main() {
    ll n;
    std::cin >> n;

    vvll g(n);
    vll qs(n, 0);
    for (ll i = 0; i < n; i++) {
        g[i].assign(0, 0);
    }

    for (ll i = 0; i < n; i++) {
        ll pi;
        std::cin >> pi >> qs[i];
        pi--;
        if (pi != -1)
            g[pi].push_back(i);
    }

    vll dp1(n, 0);
    vll dp2(n, 0);
    solve(0, dp1, dp2, g, qs);

    for (ll e : dp1)
        std::cout << e << " ";
    std::cout << "\n";
    for (ll e : dp2)
        std::cout << e << " ";
    std::cout << "\n";

    std::cout << std::max(dp1[0], dp2[0]) << "\n";
}

