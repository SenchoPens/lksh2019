#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;

const char ALPHABET_SIZE = 26;
const char ASCII_OFFSET = 'a';
using TrieGoVertex = std::array<ll, ALPHABET_SIZE>;
using TrieGo = std::vector<TrieGoVertex>;

class Trie {
    private:
        vll size;
        void _build_rank(ll v);
    public:
        TrieGo go;
        vll rank;
        std::vector<bool> term;

        Trie();
        void add(const std::string& s);
        void build_rank();
};

Trie::Trie() : go(1), size(1, 0), term(1, false), rank(0) {
    go[0].fill(-1);
}

void Trie::add(const std::string& s) {
    ll v = 0;
    for (char ch : s) {
        char c = ch - ASCII_OFFSET;
        if (go[v][c] == -1) {
            ll u = go.size();

            go.push_back(TrieGoVertex());
            go[u].fill(-1);

            go[v][c] = u;
            size.push_back(0);
            term.push_back(false);
        }
        size[v]++;
        v = go[v][c];
    }

    size[v]++;
    term[v] = true;
}

void Trie::_build_rank(ll v) {
    for (char i = 0; i < ALPHABET_SIZE; i++) {
        ll u = go[v][i];
        if (u == -1)
            continue;
        _build_rank(u);
        rank[v] = std::max(rank[u], rank[v]);
    }
    rank[v]++;
}

void Trie::build_rank() {
    rank.assign(go.size(), 0);
    _build_rank(0);
}


void solve(const Trie& trie, ll v, std::string& commands) {
    if (trie.term[v]) {
        commands.push_back('P');
    }
    std::vector<char> order(0);
    for (char i = 0; i < ALPHABET_SIZE; i++) {
        if (trie.go[v][i] != -1)
            order.push_back(i);
    }
    std::sort(order.begin(), order.end(), 
            [&trie, v](char i, char j) 
                { return trie.rank[trie.go[v][i]] < trie.rank[trie.go[v][j]]; });
    for (char i : order) {
        ll u = trie.go[v][i];
        if (u == -1)
            continue;
        commands.push_back(i + ASCII_OFFSET);
        solve(trie, u, commands);
    }
    commands.push_back('-');
}

int main() {
    ll n;
    std::cin >> n;

    Trie trie;
    for (ll i = 0; i < n; i++) {
        std::string s;
        std::cin >> s;
        trie.add(s);
    }
    trie.build_rank();

    std::string commands = "";
    solve(trie, 0, commands);

    ll si = commands.size() - 1;
    while (commands[si] == '-') {
        si--;
    }

    std::cout << si + 1 << "\n";
    for (ll i = 0; i <= si; i++) {
        std::cout << commands[i] << "\n";
    }

    return 0;
}

