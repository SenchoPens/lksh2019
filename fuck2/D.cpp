#include <bits/stdc++.h>

using ld = double;
using vint = std::vector<int>;

const ld EPS = 10e-5;

constexpr ld pi() { return std::atan(1) * 4; };

struct Point {
    int x, y;

    Point(int xx, int yy) : x(xx), y(yy) {}
    Point() { x = 0; y = 0; }

    Point operator+(const Point p) {
        return Point{x + p.x, y + p.y};
    }

    Point operator-(const Point p) {
        return Point{x - p.x, y - p.y};
    }

    int operator%(const Point p) {
        return x * p.y - y * p.x;
    }

    int operator*(const Point p) {
        return x * p.x + y * p.y;
    }

    Point operator*(int k) {
        return Point{x * k, y * k};
    }

    bool operator<(const Point& p) const {
        return std::make_pair(y, x) < std::make_pair(p.y, p.x);
    }
};

using Points = std::vector<Point>;

ld length(Point p) {
    return std::hypot(p.x, p.y);
}

Point read_point() {
    int x, y;
    std::cin >> x >> y;
    return Point(x, y);
}

std::tuple<ld, ld, ld> get_sides(Point A, Point B) {
    ld a = length(A);
    ld b = length(B);
    ld c = length(A - B);

    return std::make_tuple(a, b, c);
}

ld get_angle(Point A, Point C, Point B) {
    A = A - C;
    B = B - C;
    ld a, b, c;
    std::tie(a, b, c) = get_sides(A, B);

    ld cos = (std::pow(a, 2) + std::pow(b, 2) - std::pow(c, 2)) / (2 * a * b);
    cos = std::min(std::max(cos, -1.0), 1.0);
    return std::acos(cos);
}

ld get_area(Point A, Point B) {
    ld a, b, c;
    std::tie(a, b, c) = get_sides(A, B);

    ld p = (a + b + c) / 2;
    return std::sqrt(p * (p - a) * (p - b) * (p - c));
}

bool is_zero(ld x) {
    return std::abs(x) < EPS;
}

bool is_dumb(ld x) {
    return std::abs(x) > (pi() / 2);
}

int main() {
    Point A = read_point();
    Point B = read_point();
    Point C = read_point();

    int l;
    std::cin >> l;

    ld a, b, c;
    std::tie(a, b, c) = get_sides(A - C, B - C);

    ld max, min;
    if (is_zero(c)) {
        max = min = a;
    } else {
        max = std::max(a, b);
        if (is_zero(a) || is_zero(b)) {
            min = 0;
        } else {
            ld CAB = get_angle(C, A, B);
            ld CBA = get_angle(C, B, A);
            
            if (is_dumb(CAB) || is_dumb(CBA)) {
                min = std::min(a, b);
            } else {
                ld area = get_area(A - C, B - C);
                min = (area * 2) / c;
            }
        }
    }

    std::cout << std::fixed;
    std::cout << std::setprecision(7);

    min -= l;
    max -= l;
    min = std::max(0.0, min);
    max = std::max(0.0, max);
    std::cout << min << "\n";
    std::cout << max << "\n";
}

