#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;

ll solve(const vll& p) {
    ll n = p.size();
    ll c = 0;
    for (ll i = n - 1; i >= 0; i--) {
        ll k = i + 1 - p[i];
        if ((i + 1) % k == 0)
            return k;
        if (p[n - 1] == c)
            return i + 1;
        c++;
    }
    return n;
}

int main() {
    std::string s;
    std::cin >> s;

    ll n = s.size();
    vll p(n, 0);

    for (ll i = 1; i < n; i++) {
        ll cur = p[i - 1];
        while (cur > 0 && s[i] != s[cur]) {
            cur = p[cur - 1];
        }
        if (s[i] == s[cur])
            cur++;
        p[i] = cur;
    }

    std::cout << solve(p) << "\n";
}

