#include <bits/stdc++.h>

using ll = long long;
using ull = unsigned long long;
using vll = std::vector<ll>;
using vull = std::vector<ull>;

const ull ASCII_OFFSET = 96;

const ull P1 = 37;
const ull MOD1 = 1000000000 + 7;
const ull P2 = 43;
const ull MOD2 = 1000000000 + 9;


class HashString {
        vull hashes;
        ull p;
        ull q;
        vull ps;
        ull get_hash(ll l, ll r);
    public:
        HashString(std::string& s, ull p, ull q);
        bool is_equal(ll l1, ll r1, ll l2, ll r2);
};

HashString::HashString(std::string& s, ull pp, ull qq) : p(pp), q(qq) {
    ll n = s.size();
    hashes.assign(n + 1, 0);
    for (ll i = 1; i < n + 1; i++) {
        ull c = static_cast<ull>(s[i - 1]) - ASCII_OFFSET;
        hashes[i] = (c + hashes[i - 1] * p % q) % q;
    }

    ps.assign(n + 1, 1);
    for (ll i = 1; i < n + 1; i++) {
        ps[i] = ps[i - 1] * p % q;
    }
}

ull HashString::get_hash(ll l, ll r) {
    return (hashes[r + 1] + q - hashes[l] * ps[r - l + 1] % q) % q;
}

bool HashString::is_equal(ll l1, ll r1, ll l2, ll r2) {
    return get_hash(l1, r1) == get_hash(l2, r2);
}

using Hashes = std::vector<HashString*>;

bool is_equal(const Hashes& hs, ll a, ll b, ll c, ll d) {
    return std::all_of(hs.begin(), hs.end(), [a, b, c, d](auto p1) { return p1->is_equal(a, b, c, d); });
}

bool is_repeated(const Hashes& hs, ll n, ll k) {
    bool f = true;
    for (ll i = 1; i < (n / k); i++) {
        f = f && is_equal(hs, i * k, (i + 1) * k - 1, 0, k - 1);
    }
    ll r = n % k;
    return f && is_equal(hs, 0, r - 1, n - r, n - 1);
}

int main() {
    std::string s;
    std::cin >> s;

    Hashes hs = {new HashString(s, P1, MOD1), new HashString(s, P2, MOD2)};

    ll n = s.size();

    std::cout << is_equal(hs, 0, n - 1, 0, n - 1) << "\n";
    for (ll i = 1; i < n + 1; i++) {
    std::cout << i << " " << is_repeated(hs, n, i) << "\n";
    }

    vll indices(n);
    std::iota(indices.begin(), indices.end(), 1);
    std::cout << *std::lower_bound(indices.begin(), indices.end(), n,
                     [&hs, n](ll i, ll j) { return (!is_repeated(hs, n, i)) && is_repeated(hs, n, j); })
              << "\n";
}

