#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;

int main() {
    std::string s;
    std::cin >> s;

    ll n = s.size();
    vll z(n, 0);
    z[0] = n;

    for (ll i = 1, l = 0, r = 0; i < n; i++) {
        if (i <= r)
            z[i] = std::min(r - i + 1, z[i - l]);
        while (i + z[i] < n && z[i] < n && s[i + z[i]] == s[z[i]])
            z[i]++;
        if (i + z[i] - 1 > r) {
            l = i;
            r = i + z[i] - 1;
        }
    }

    for (auto el : z) {
        std::cout << el << " ";
    }
    std::cout << "\n";

    ll i = n - 1;
    while (z[i] == z[i - 1] + 1) {
        i--;
    }
    
    std::cout << i + 1 << "\n";
}
