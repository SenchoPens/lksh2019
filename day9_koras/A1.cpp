#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <set>
#include <map>
#include <stack>

using namespace std;

vector<map<int, int>> go;
vector<int> term;
vector<int> t;


void add(string &s) {
    int ind = 0;
    for (auto u : s) {
        char ch = u - 'a';
        if (go[ind].find(ch) == go[ind].end()) {
            go.push_back(map<int, int>());
            term.push_back(0);
            t.push_back(0);
            go[ind][ch] = go.size() - 1;
        }
        t[ind]++;
        ind = go[ind][ch];
    }
    t[ind]++;
    term[ind] = 1;
}

void find(string &s, int k, int ind) {
    int b = 0;
    if (k == 1 && term[ind]) {
        return;
    }
    if (term[ind]) {
        k--;
    }
    for (auto u : go[ind]) {
        b += t[u.second];
        if (k <= b && k > b - t[u.second]) {
            s.push_back((char) u.first + 'a');
            find(s, k - (b - t[u.second]), u.second);
            break;
        }
    }
}

int main() {
    int q;
    cin >> q;
    go.push_back(map<int, int>());
    term.push_back(0);
    t.push_back(0);
    for (int i = 0; i < q; i++) {
        string s;
        cin >> s;
        if (s[0] >= '0' && s[0] <= '9') {
            string ans;
            int a = stoi(s);
            find(ans, a, 0);
            cout << ans << endl;
        } else {
            add(s);
        }
    }
    return 0;
}
