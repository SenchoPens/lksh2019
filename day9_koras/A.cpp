#include <bits/stdc++.h>

using ll = long long;
using vll = std::vector<ll>;

const char ALPHABET_SIZE = 26;
const char ASCII_OFFSET = 'a';
using TrieGoVertex = std::array<ll, ALPHABET_SIZE>;
using TrieGo = std::vector<TrieGoVertex>;

class Trie {
    private:
        TrieGo go;
        vll size;
        std::vector<bool> term;
    public:
        Trie();
        void add(const std::string& s);
        void get_kth(ll v, ll k, std::string& s);
};

Trie::Trie() : go(1), size(1, 0), term(1, false) {
    go[0].fill(-1);
}

void Trie::add(const std::string& s) {
    ll v = 0;
    for (char ch : s) {
        char c = ch - ASCII_OFFSET;
        if (go[v][c] == -1) {
            ll u = go.size();

            go.push_back(TrieGoVertex());
            go[u].fill(-1);

            go[v][c] = u;
            size.push_back(0);
            term.push_back(false);
        }
        size[v]++;
        v = go[v][c];
    }

    size[v]++;
    term[v] = true;
}

void Trie::get_kth(ll v, ll k, std::string& s) {
    if (term[v] && k == 1)
        return;

    if (term[v])
        k--;

    ll cur_size = 0;
    for (char i = 0; i < ALPHABET_SIZE; i++) {
        ll u = go[v][i];
        if (u == -1)
            continue;
        cur_size += size[u];
        if (k <= cur_size && k > cur_size - size[u]) {
            s.push_back(static_cast<char>(i + ASCII_OFFSET));
            get_kth(u, k - (cur_size - size[u]), s);
            return;
        }
    }
}

int main() {
    ll n;
    std::cin >> n;

    Trie trie;
    for (ll i = 0; i < n; i++) {
        std::string s;
        std::cin >> s;
        if (s[0] >= '0' && s[0] <= '9') {
            ll k = std::stoll(s);
            std::string ans;
            trie.get_kth(0, k, ans);
            std::cout << ans << "\n";
        } else {
            trie.add(s);
        }
    }
    return 0;
}
